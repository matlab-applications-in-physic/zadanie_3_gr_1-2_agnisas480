//written in Scilab 
//Author: Agnieszka Sasiela
//Subject: Matlab applications in physics, task 3
//The aim of this program was to open and read csv files with the data about the air quality in one of Polish towns, find the maximum value of PM10 in analyzed data from one week
// and also check if there is a statistiacal difference between the average value of PM10 during the days and nights

// Please change the path to the files on your computer
path_to_D1='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-08.csv'
path_to_D2='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-09.csv'
path_to_D3='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-10.csv'
path_to_D4='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-11.csv'
path_to_D5='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-12.csv'
path_to_D6='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-13.csv'
path_to_D7='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\dane-pomiarowe_2019-10-14.csv'
path_to_generated_file='C:\Users\agabi\Desktop\studia\matlab\program do importu plików\data\data\PMresults.dat'
// First step is to read each of 7 files to 6 columns, as it is saved in original csv. The columns consists of strings.
D1= csvRead(path_to_D1,ascii(59), [], 'string');
D2= csvRead(path_to_D2,ascii(59), [], 'string');
D3= csvRead(path_to_D3,ascii(59), [], 'string');
D4= csvRead(path_to_D4,ascii(59), [], 'string');
D5= csvRead(path_to_D5,ascii(59), [], 'string');
D6= csvRead(path_to_D6,ascii(59), [], 'string');
D7= csvRead(path_to_D7,ascii(59), [], 'string');
// I need to analyse only data from the first and 6th column (time + PM10). The X contains data from 1st column in every from 7 files and Y from 6th column in every from 7 files.
X_1= D1(:,6);
Y_1=D1(:,1);
X_2= D2(:,6);
Y_2=D2(:,1);
X_3= D3(:,6);
Y_3=D3(:,1);
X_4= D4(:,6);
Y_4=D4(:,1);
X_5= D5(:,6);
Y_5=D5(:,1);
X_6= D6(:,6);
Y_6=D6(:,1);
X_7= D7(:,6);
Y_7=D7(:,1);
//n is the number of rows in columns with needed data
n = size(X_1, "r");
// These "zeros" matrix is needed to add up the data - PM10 values to the accurate time (time when it was measured)
c_1=zeros(n,2);
c_2=zeros(n,2);
c_3=zeros(n,2);
c_4=zeros(n,2);
c_5=zeros(n,2);
c_6=zeros(n,2);
c_7=zeros(n,2);
// In this for loop I added the PM10 values to the 2nd column of c matrix ( for every of 7 files)
for k=1:n
    c_1(k,2)=strtod(X_1(k));
    c_2(k,2)=strtod(X_2(k));
    c_3(k,2)=strtod(X_3(k));
    c_4(k,2)=strtod(X_4(k));
    c_5(k,2)=strtod(X_5(k));
    c_6(k,2)=strtod(X_6(k));
    c_7(k,2)=strtod(X_7(k));
end 
// In this for loop I add the time values to the 1st column of c matrix ( for every of 7 files)
for k=1:n
    c_1(k,1)=strtod(Y_1(k));
    c_2(k,1)=strtod(Y_2(k));
    c_3(k,1)=strtod(Y_3(k));
    c_4(k,1)=strtod(Y_4(k));
    c_5(k,1)=strtod(Y_5(k));
    c_6(k,1)=strtod(Y_6(k));
    c_7(k,1)=strtod(Y_7(k));
end 
//The last 3 rows in c matrix are useless so I "cut them off"
c_1=resize_matrix(c_1,25,2);
c_2=resize_matrix(c_2,25,2);
c_3=resize_matrix(c_3,25,2);
c_4=resize_matrix(c_4,25,2);
c_5=resize_matrix(c_5,25,2);
c_6=resize_matrix(c_6,25,2);
c_7=resize_matrix(c_7,25,2);
// I look for the maximum value of PM10 for every 24h
MX_1=max(c_1(:,2));
MX_2=max(c_2(:,2));
MX_3=max(c_3(:,2));
MX_4=max(c_4(:,2));
MX_5=max(c_5(:,2));
MX_6=max(c_6(:,2));
MX_7=max(c_7(:,2));
// counters needed to count in which row the maximus is because I've got to know the time of max value of PM10
counter_1= 0;
counter_2= 0;
counter_3= 0;
counter_4= 0;
counter_5= 0;
counter_6= 0;
counter_7= 0;
// in following loops the process of looking fo the time of max PM10  is taking place. This loop is checking in which row is the max value. 
for l=1:25
v_1=c_1(l,2)==MX_1;
    if v_1==%F;
    counter_1= counter_1 +1
    else
    break
    end
end

for l=1:25
v_2=c_2(l,2)==MX_2;
    if v_2==%F;
    counter_2= counter_2 +1;
    else
    break
    end
end

for l=1:25
v_3=c_3(l,2)==MX_3;
    if v_3==%F;
    counter_3= counter_3 +1;
    else
    break
    end
end

for l=1:25
v_4=c_4(l,2)==MX_4;
    if v_4==%F;
    counter_4= counter_4 +1;
    else
    break
    end
end

for l=1:25
v_5=c_5(l,2)==MX_5;
    if v_5==%F;
    counter_5= counter_5 +1;
    else
    break
    end
end

for l=1:25
v_6=c_6(l,2)==MX_6;
    if v_6==%F;
    counter_6= counter_6 +1;
    else
    break
    end
end

for l=1:25
v_7=c_7(l,2)==MX_7;
    if v_7==%F;
    counter_7= counter_7 +1;
    else
    break
    end
end
//The p matrix cosists of maximum value of PM10 dor every 24h in 1st column and the hour, in which the max PM10  occured.
p=zeros(6,2)
p(1,1)=MX_1;
p(2,1)=MX_2;
p(3,1)=MX_3;
p(4,1)=MX_4;
p(5,1)=MX_5;
p(6,1)=MX_6;
p(7,1)=MX_7;
p(1,2)=counter_1;
p(2,2)=counter_2;
p(3,2)=counter_3;
p(4,2)=counter_4;
p(5,2)=counter_5;
p(6,2)=counter_6;
p(7,2)=counter_7
//I look for the max PM10 from the whole week 
pm_max=max(p(:,1))
// Analogously like above look for row in which the max PM10 is 
counter=1.
for l=1:7
q=p(l,1)==pm_max;
    if q==%F;
    counter= counter +1
    else
    break
    end
end
counter
//Good_hour is the hour of max PM 10 in whole week 
good_hour=p(counter,2)
//mould the the time nad value of max PM10 in whole week to its date 
if counter ==1.  
   the_date=datenum(2019,10,08);
elseif counter==2. 
   the_date=datenum(2019,10,08)+1;
elseif counter==3. 
   the_date=datenum(2019,10,08)+2;
elseif counter==4. 
   the_date=datenum(2019,10,08)+3;
elseif counter==5. 
   the_date=datenum(2019,10,08)+4;
elseif counter==6. 
   the_date=datenum(2019,10,08)+5;
else counter==7. 
   the_date=datenum(2019,10,08)+6;
end
// datevec converts a serial date number (defined by datenum) to a date vector V having elements [year, month, day, hour, minute, second]. 
good_date=datevec(the_date);
//In following I cutt off the last 3 values- hour, minute and second 
good_date=resize_matrix(good_date,1,3);
// The hour of max PM10 
hour=string(p(counter,2));
hour_1= hour + ":00";
// All information about the data will be pritned in the end of my program 
//That's the last part- computing the statistical difference 
c_days=zeros(84,1);
c_nigths=zeros(83,1);
// c_days matrix are for values of PM10 for every days from 7:00 to 18:00 ( 7am to 6 pm )
// c_nights matrix are for values of PM10 for every night from 1:00 to 6:00 ( 1am to 6 am ) and from 19:00 do 24:00 ( 7pm to midnight)
for i=1:12
c_days(i,1)=c_1(i+7,2);
c_days(i+12,1)=c_2(i+7,2);
c_days(i+24,1)=c_3(i+7,2);
c_days(i+36,1)=c_4(i+7,2);
c_days(i+48,1)=c_5(i+7,2);
c_days(i+60,1)=c_6(i+7,2);
c_days(i+72,1)=c_7(i+7,2);
end
//The average value of PM10 during the days 
mean_PM_days=mean(c_days);

for i=1:6
c_nights(i,1)=c_1(i+1,2);

c_nights(i+6,1)=c_1(i+19,2);

c_nights(i+12,1)=c_2(i+1,2);

c_nights(i+18,1)=c_2(i+19,2);

c_nights(i+24,1)=c_3(i+1,2);

c_nights(i+30,1)=c_3(i+19,2);

c_nights(i+36,1)=c_4(i+1,2);

c_nights(i+42,1)=c_4(i+19,2);

c_nights(i+48,1)=c_5(i+1,2);

c_nights(i+54,1)=c_5(i+19,2);

c_nights(i+60,1)=c_6(i+1,2);

c_nights(i+66,1)=c_6(i+19,2);

c_nights(i+72,1)=c_7(i+1,2);

c_nights(i+78,1)=c_7(i+19,2);

end
//The average value of PM10 during the nights
mean_PM_nights=mean(c_nights);
//computing the standard deviation  for days
s_d_of_PM_days= stdev(c_days);
//computing the standard deviation  for nights
s_d_of_PM_nights= stdev(c_nights);
// t is the statistical test.
numerator= abs(mean_PM_days - mean_PM_nights);
denominator= sqrt( s_d_of_PM_days**2 + s_d_of_PM_nights**2);
t=numerator / denominator;
//printing the information 
data_to_print_2= " During the analysed period of time(from Tuesday 8th October 2019 to Monday 14th October 2019) the maximum value of PM was: " + string(pm_max) +" This value was registered at " + string(hour_1) + " on "+ string(good_date(1)) +":"+ string(good_date(2))+ ":"+ string(good_date(3)) + ".""The statistical test verifies whether the difference beetween the average value of PM10 during the days and the average value of PM10 during the nights in this week is statistically significant. If the calculated value- t is smaller than 2 then there is no relevant difference. If the value is bigger than 3 the difference is substantial. In this case the result from the test is:  " + string(t) +string(r)
//Checking the results of the statistical test
r=0
if t < 2
r='The difference is statisically insignificant.';
else;
r='The difference is statisically significant.';
end
//printing information to a file 
r2 = mputl(data_to_print_2, path_to_generated_file )
info=0;
if r2==%T;
info='The data were succesfully saved in the generated file.';
else;
info='The date were not succesfully saved in the generated file.';
end
// giving the information about the file
disp(good_date, "on", hour_1, "This value was registered at ",pm_max,"During the analysed period of time(from Tuesday 8th October 2019 to Monday 14th October 2019) the maximum value of PM10 was: ") 
disp(string(r), string(t), "This statistical test verifies whether the difference beetween the average value of PM10 during the days and the average value of PM10 during the nights in this week is statistically significant. If the calculated value- t is smaller than 2 then there is no relevant difference. If the value is bigger than 3 the difference is substantial. In this case the result from the test is:  ") 
disp(info)
